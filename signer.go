package goproxy

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"github.com/patrickmn/go-cache"
	"math/big"
	"net"
	"sort"
	"strings"
	"sync/atomic"
	"time"
)

func hashSorted(lst []string) []byte {
	c := make([]string, len(lst))
	copy(c, lst)
	sort.Strings(c)
	h := sha1.New()
	for _, s := range c {
		h.Write([]byte(s + ","))
	}
	return h.Sum(nil)
}

func hashSortedBigInt(lst []string) *big.Int {
	rv := new(big.Int)
	rv.SetBytes(hashSorted(lst))
	return rv
}

var goproxySignerVersion = ":goroxy1"

var certprivRSA crypto.Signer
var certprivECDSA crypto.Signer
var crtCache = cache.New(20*time.Minute, 10*time.Minute)
var serialCA uint64 = 9999

// PR-368
func signHost(ca tls.Certificate, hosts []string) (cert *tls.Certificate, err error) {
	key := strings.Join(hosts[:], "/")
	value, exist := crtCache.Get(key)
	if exist {
		cert = value.(*tls.Certificate)
		return
	}

	cert, err = generateCertificate(ca, hosts)

	_ = crtCache.Add(key, cert, cache.DefaultExpiration)
	return
}

// PR-368
func generateCertificate(ca tls.Certificate, hosts []string) (cert *tls.Certificate, err error) {
	var x509ca *x509.Certificate

	// Use the provided ca and not the global GoproxyCa for certificate generation.
	if x509ca, err = x509.ParseCertificate(ca.Certificate[0]); err != nil {
		return
	}

	// https://github.com/elazarl/goproxy/pull/432
	start := time.Unix(time.Now().Unix()-2592000, 0) // 2592000  = 30 day
	end := time.Unix(time.Now().Unix()+31536000, 0)  // 31536000 = 365 day

	template := x509.Certificate{
		// TODO(elazar): instead of this ugly hack, just encode the certificate and hash the binary form.
		SerialNumber: new(big.Int).SetUint64(atomic.AddUint64(&serialCA, 1)),
		Issuer:       x509ca.Subject,
		Subject: pkix.Name{
			Organization: []string{"GoProxy untrusted MITM proxy Inc"},
		},
		NotBefore: start,
		NotAfter:  end,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}
	for _, h := range hosts {
		if ip := net.ParseIP(h); ip != nil {
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			template.DNSNames = append(template.DNSNames, h)
			template.Subject.CommonName = h
		}
	}

	// https://github.com/elazarl/goproxy/pull/338
	var derBytes []byte
	if derBytes, err = x509.CreateCertificate(rand.Reader, &template, x509ca, x509ca.PublicKey, ca.PrivateKey); err != nil {
		return
	}

	return &tls.Certificate{
		Certificate: [][]byte{derBytes, ca.Certificate[0]},
		PrivateKey:  ca.PrivateKey,
	}, nil
}

func init() {
	// Avoid deterministic random numbers
	serialCA = uint64(time.Now().Unix() * 1000000000)
	certprivRSA, _ = rsa.GenerateKey(rand.Reader, 2048);
	certprivECDSA, _ = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
}
