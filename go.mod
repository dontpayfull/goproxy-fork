module github.com/elazarl/goproxy

go 1.16

require (
	github.com/elazarl/goproxy/ext v0.0.0-20190711103511-473e67f1d7d2
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
)
